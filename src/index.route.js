import express from 'express';
import config from './config/config';

const router = express.Router();

router.get('/health_check', (req, res) => {
  res.status(200).send({ status: 'OK', env: config.env });
});

export default router;
