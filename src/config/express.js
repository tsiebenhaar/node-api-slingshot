import express from 'express';
import routes from '../index.route';

const app = express();

app.use('/api', routes);

export default app;
