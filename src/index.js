import chalk from 'chalk';
import app from './config/express';
import config from './config/config';

app.listen(config.port, (req, res) => {
  console.log(chalk.green(`Server running on port ${config.port}.`));
  console.log(chalk.green(`Server running on ${config.env} environment.`));
});

export default app;
